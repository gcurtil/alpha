grammar AlphaFormula;
/* * Parser Rules */
number: INT | FLOAT;

priceField: 'OPEN' | 'CLOSE' | 'HIGH' | 'LOW';
tsFuncName: 'DELAY' | 'SMA';
tsFuncApp: tsFuncName '(' priceField ',' INT ')';


expr: 
      expr POWER expr            #power
    | expr op=(MUL | DIV) expr   #mulDiv     
    | expr op=(ADD | SUB) expr   #addSub    
    | number                     #num
    | '(' expr ')'               #parens
    | priceField                 #priceFieldUse
    | tsFuncApp                  #tsFuncAppUse
    ;

/* * Lexer Rules */
fragment DIGIT: [0-9];
INT: DIGIT +;
FLOAT: DIGIT + '.' DIGIT +;

MUL: '*';
DIV: '/';
ADD: '+';
SUB: '-';
POWER: '^';
WS: (' ' | '\t' | '\r' | '\n' )+ -> skip;
