using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

using NUnit.Framework;
using Alpha.Core;
using Alpha.Util;

namespace Alpha.Tests
{
    [TestFixture]
    public class IoHelperTests
    {
        [Test]
        public void TestReadCsv()
        {
            string[] lines = new[] 
            { 
                "Date,Open,High,Low,Close,Adj Close,Volume",
                "2020-06-08,295.000000,298.799988,292.000000,292.799988,287.182831,8429072",
                "2020-06-09,293.000000,296.000000,290.399994,290.799988,285.221191,8360605",
            };
            
            using(var reader = new StringReader(String.Join(Environment.NewLine, lines)))
            {
                // Read the data and build time series
                var ts = IoHelper.ReadPriceHistoryFromCsv(reader);
                Assert.IsNotNull(ts);

                // Check we have 2 records in the time series
                Assert.AreEqual(2, ts.Series.Count);
                
                // Check the first point in the series
                var pr0 = ts.Series[0];
                Assert.AreEqual(new DateTime(2020, 6, 8), pr0.Date);
                Assert.AreEqual(295.0,      pr0.Open);
                Assert.AreEqual(292.799988, pr0.Close);
                Assert.AreEqual(292.0,      pr0.Low);
                Assert.AreEqual(298.799988, pr0.High);

                // Check the second point
                var pr1 = ts.Series[1];
                Assert.AreEqual(new DateTime(2020, 6, 9), pr1.Date);
                Assert.AreEqual(293.0,      pr1.Open);
                Assert.AreEqual(290.799988, pr1.Close);
                Assert.AreEqual(290.399994, pr1.Low);
                Assert.AreEqual(296.0,      pr1.High);
            }            
        }    

        // WriteAlphaTsToCsv    
        [Test]
        public void TestWriteCsv()
        {
            var records = new List<AlphaRecord>();
            records.Add(new AlphaRecord()
            {
                Date = new DateTime(2020, 6, 8),
                Alphas = new List<double>{ 1.0, 2.0 },
            });
            records.Add(new AlphaRecord()
            {
                Date = new DateTime(2020, 6, 9),
                Alphas = new List<double>{ 0.0, -1.0 },
            });

            var ts = new AlphaTS(records);
            using(var writer = new StringWriter())
            {
                IoHelper.WriteAlphaTsToCsv(ts, writer);
                writer.Flush();
                var strContent = writer.ToString();
                var lines = new List<string>(strContent.Split(Environment.NewLine));
                lines = lines.Where(s => !string.IsNullOrWhiteSpace(s)).ToList();

                Console.WriteLine(lines);

                Assert.AreEqual(3, lines.Count);
                Assert.AreEqual("Date,Alpha1,Alpha2",   lines[0]);
                Assert.AreEqual("2020-06-08,1,2",       lines[1]);
                Assert.AreEqual("2020-06-09,0,-1",      lines[2]);
            }            
        }

        [Test]
        public void TestReadFormulaDefinitions()
        {
            string[] lines = new[] 
            { 
                "# Test definitions",
                "ALPHA1 = CLOSE – DELAY(CLOSE, 5)",
                "# next one",
                "ALPHA2 = OPEN – DELAY(CLOSE, 5)",
                "",
                "ALPHA3 = (HIGH * LOW)^0.5 – SMA(CLOSE, 5)",
                "",
            };
            
            using(var reader = new StringReader(String.Join(Environment.NewLine, lines)))
            {
                // Read the formula definitions 
                var defs = IoHelper.ReadFormulaDefinitions(reader);
                Assert.IsNotNull(defs);
                
                Assert.AreEqual(3, defs.Count);
                Assert.AreEqual(Tuple.Create("ALPHA1", "CLOSE – DELAY(CLOSE, 5)"), defs[0]);
                Assert.AreEqual(Tuple.Create("ALPHA2", "OPEN – DELAY(CLOSE, 5)"), defs[1]);
                Assert.AreEqual(Tuple.Create("ALPHA3", "(HIGH * LOW)^0.5 – SMA(CLOSE, 5)"), defs[2]);
            }
        }
    }
}