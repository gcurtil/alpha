using System;

// Needed because of the files generated by ANTLR
// https://stackoverflow.com/questions/8590402/antlr-for-c-sharp-and-clscompliant-attribute
[assembly: CLSCompliant(false)]
