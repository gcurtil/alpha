using Alpha.Core;

namespace Alpha.Functions
{
    public class DelayTsFunctionCalculator : TsFunctionCalculatorBase, ITsFunctionCalculator
    {
        public DelayTsFunctionCalculator(PriceField field, int intArg)
            : base(field, intArg)
        {
        }

        public int MaxLag()
        {
            return IntArg;
        }

        public double Eval(PriceTS ts, int index)
        {
            // how far back we have to read values from the time series
            int lag = IntArg;
            checkForValidIndex(index, lag);

            var effectiveIndex = index - lag;            
            var price = ts.Series[effectiveIndex];
            return FormulaHelper.ReadField(price, Field);
        }

        public void AddNewPoint(PriceRecord point)
        {
            Buffer.Add(point);
            while (Buffer.Count > MaxLag() + 1)
            {
                Buffer.RemoveAt(0);
            }
        }

        public double CurrentValue()
        {
            // use the first element in the buffer
            var price = Buffer[0];
            return FormulaHelper.ReadField(price, Field);
        }
    }
        
}