using System;
using Alpha.Core;
using Alpha.FormulaTree;
using Alpha.Functions;
using Alpha.Generated;

namespace Alpha
{
    public class VisitResult 
    {
        public IFormulaNode RootNode {get; set;}

        public override string ToString() 
        {
            return String.Format("Root: {0}", RootNode);
        }
    }

   public class MyAlphaFormulaVisitor : AlphaFormulaBaseVisitor<VisitResult>
   {
        public override VisitResult VisitNum(AlphaFormulaParser.NumContext context)
        {
            var numValue = double.Parse(context.GetText());
            var node = new NumberFormulaNode(){ NumValue = numValue} ;            
            var result = new VisitResult() { RootNode = node };
            return result;
        }        
        public override VisitResult VisitPriceFieldUse(AlphaFormulaParser.PriceFieldUseContext context)
        {
            var fieldName = context.GetText();
            var field = Enum.Parse<PriceField>(fieldName);
            var node = new PriceFieldFormulaNode() { Field = field };

            base.VisitPriceFieldUse(context);
            var result = new VisitResult() { RootNode = node };
            return result;
        }

        public override VisitResult VisitParens(AlphaFormulaParser.ParensContext context)
        {
            return Visit(context.expr());
        }

        private VisitResult CreateBinaryOpFormulaNode(VisitResult left, VisitResult right, BinaryOperation op)
        {            
            var node = new BinaryOpFormulaNode(left.RootNode, right.RootNode, op);
            var result = new VisitResult() { RootNode = node };
            return result;   
        }

        public override VisitResult VisitPower(AlphaFormulaParser.PowerContext context)
        {
            // get left and right operands
            var left = Visit(context.expr(0));
            var right = Visit(context.expr(1));

            return CreateBinaryOpFormulaNode(left, right, BinaryOperation.POWER);
        }

        public override VisitResult VisitMulDiv(AlphaFormulaParser.MulDivContext context)
        {
            // get left and right operands
            var left = Visit(context.expr(0));
            var right = Visit(context.expr(1));            
            BinaryOperation op = context.op.Type == AlphaFormulaParser.MUL ? BinaryOperation.MUL : BinaryOperation.DIV;
            
            return CreateBinaryOpFormulaNode(left, right, op);
        }

        public override VisitResult VisitAddSub(AlphaFormulaParser.AddSubContext context)
        {
            // get left and right operands
            var left = Visit(context.expr(0));
            var right = Visit(context.expr(1));
            BinaryOperation op = context.op.Type == AlphaFormulaParser.ADD ? BinaryOperation.ADD : BinaryOperation.SUB;

            return CreateBinaryOpFormulaNode(left, right, op);
        }

        public override VisitResult VisitTsFuncAppUse(AlphaFormulaParser.TsFuncAppUseContext context) 
        { 
            var ctx = context.tsFuncApp();
            var funcName = ctx.tsFuncName().GetText();
            var func = Enum.Parse<TsFunction>(funcName);
            var fieldName = ctx.priceField().GetText();
            var field = Enum.Parse<PriceField>(fieldName);
            var intArg = int.Parse(ctx.INT().GetText());
            
            var node = new TsFunctionFormulaNode(func, field, intArg);
            var result = new VisitResult() { RootNode = node };
            return result;
        }
    }

}