using System;
using Alpha.Core;

namespace Alpha.FormulaTree
{
    public class NumberFormulaNode : FormulaNodeBase, IFormulaNode
    {
        public virtual double Eval(PriceTS tS, int index)
        {
            return NumValue;
        }

        public virtual int MaxLag()
        {
            return 0;
        }

        public double NumValue { get; set; }

        public virtual void AddNewPoint(PriceRecord point)
        {
            // nothing to do
        }

        public virtual double CurrentValue()
        {
            return NumValue;
        }

        public override string ToString()
        {
            return string.Format("[Number: {0}]", NumValue);
        }
    }

}