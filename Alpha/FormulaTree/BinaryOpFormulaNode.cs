using System;
using Alpha.Core;

namespace Alpha.FormulaTree
{
    public class BinaryOpFormulaNode : FormulaNodeBase, IFormulaNode
    {

        public BinaryOpFormulaNode(IFormulaNode leftOperand, IFormulaNode rightOperand, BinaryOperation operation)
        {
            this.LeftOperand = leftOperand;
            this.RightOperand = rightOperand;
            this.Operation = operation;

            this.Children.Add(this.LeftOperand);
            this.Children.Add(this.RightOperand);
        }

        public IFormulaNode LeftOperand { get; private set; }
        public IFormulaNode RightOperand { get; private set; }
        public BinaryOperation Operation { get; private set; }

        public virtual int MaxLag()
        {
            return Math.Max(LeftOperand.MaxLag(), RightOperand.MaxLag());
        }


        public static double CalcBinary(double leftValue, double rightValue, BinaryOperation op)
        {
            switch (op)
            {
                case BinaryOperation.ADD:
                    return leftValue + rightValue;
                case BinaryOperation.SUB:
                    return leftValue - rightValue;
                case BinaryOperation.MUL:
                    return leftValue * rightValue;
                case BinaryOperation.DIV:
                    return leftValue / rightValue;
                case BinaryOperation.POWER:
                    return Math.Pow(leftValue, rightValue);
                default:
                    return 0.0; // Not reached
            }
        }

        public virtual double Eval(PriceTS ts, int index)
        {
            var leftVal = LeftOperand.Eval(ts, index);
            var rightVal = RightOperand.Eval(ts, index);
            var res = CalcBinary(leftVal, rightVal, Operation);
            return res;
        }

        public virtual void AddNewPoint(PriceRecord point)
        {
            // Design choice: we can either propagate the update to child nodes
            // here, or leave the logic to walk the tree and propagate the update
            // to a separate class.
            
            // In the first case, this method is a no-op, in the second case we 
            // simply call AddNewPoint() on our two child nodes here.
            LeftOperand.AddNewPoint(point);
            RightOperand.AddNewPoint(point);
        }

        public virtual double CurrentValue()
        {
            var leftVal = LeftOperand.CurrentValue();
            var rightVal = RightOperand.CurrentValue();
            var res = CalcBinary(leftVal, rightVal, Operation);
            return res;
        }

        public override string ToString()
        {
            return string.Format("[BinaryOp: {0}({1}, {2})]",
                                 Operation, LeftOperand, RightOperand);
        }
    }

}