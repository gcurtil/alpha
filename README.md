### Creating the project files ###

```
gui@monolith:~/devel/nom01$ dotnet new sln 

gui@monolith:~/devel/nom01$ mkdir Alpha
gui@monolith:~/devel/nom01$ mkdir Alpha.Tests 

gui@monolith:~/devel/nom01$ cd Alpha
gui@monolith:~/devel/nom01/Alpha$ dotnet new console

gui@monolith:~/devel/nom01/Alpha$ cd ..
gui@monolith:~/devel/nom01$ cd Alpha.Tests/
gui@monolith:~/devel/nom01/Alpha.Tests$ dotnet new nunit 

gui@monolith:~/devel/nom01/Alpha.Tests$ dotnet add reference ../Alpha/Alpha.csproj 
Reference `..\Alpha\Alpha.csproj` added to the project.

gui@monolith:~/devel/nom01/Alpha.Tests$ cd ..
gui@monolith:~/devel/nom01$ dotnet sln add Alpha/Alpha.csproj 
Project `Alpha/Alpha.csproj` added to the solution.
gui@monolith:~/devel/nom01$ dotnet sln add Alpha.Tests/Alpha.Tests.csproj 
Project `Alpha.Tests/Alpha.Tests.csproj` added to the solution.

```

```
gui@monolith:~/devel/nom01$ mkdir Alpha.CmdLine
gui@monolith:~/devel/nom01$ cd Alpha.CmdLine/
gui@monolith:~/devel/nom01/Alpha.CmdLine$ dotnet new console 

gui@monolith:~/devel/nom01/Alpha.CmdLine$ dotnet add reference ../Alpha/Alpha.csproj 
Reference `..\Alpha\Alpha.csproj` added to the project.

gui@monolith:~/devel/nom01/Alpha.CmdLine$ cd ..
gui@monolith:~/devel/nom01$ dotnet sln add Alpha.CmdLine/Alpha.CmdLine.csproj 
Project `Alpha.CmdLine/Alpha.CmdLine.csproj` added to the solution.

```

For command line parsing, adding the Beta `System.CommandLine` from Microsoft.
See: https://github.com/dotnet/command-line-api/blob/main/docs/Your-first-app-with-System-CommandLine.md

```
gui@monolith:~/devel/nom01/Alpha.CmdLine$ dotnet add package System.CommandLine --prerelease 

```


### Generate the grammar ###

#### Install ANTLR 4

From: https://github.com/antlr/antlr4/blob/master/doc/getting-started.md

```
gui@monolith:~/devel/nom01$ curl -O https://www.antlr.org/download/antlr-4.9-complete.jar 
```

Generate the parser code:
```
gui@monolith:~/devel/nom01$ java -jar ./antlr-4.9-complete.jar Alpha/AlphaFormula.g4 -o Alpha/Generated -Dlanguage=CSharp -no-listener -visitor -package Alpha.Generated 
```

Regenerate:
```
gui@monolith:~/devel/nom01$ java -jar ./antlr-4.9-complete.jar Alpha/AlphaFormula.g4 -o Alpha/Generated -Dlanguage=CSharp -no-listener -visitor -package Alpha.Generated 
```

### Docker

https://docs.microsoft.com/en-us/dotnet/core/docker/build-container?tabs=linux


Build the container image:
```
gui@monolith:~/devel/CLEAN/nom01$ docker build -t alpha -f Dockerfile .
```

Enter the container and inspect:
```
gui@monolith:~/devel/CLEAN/nom01$ docker run -it --rm --entrypoint "bash" alpha
```

Run the app interactively and check output:
```
root@3d3db6bd3491:/App# dotnet Alpha.dll 
result of visitor.Visit call, root: [BinaryOp: ADD([PriceField: LOW], [BinaryOp: MUL([Number: 3], [BinaryOp: POWER([PriceField: HIGH], [Number: 2])])])], maxLag: 0
Read time series with 247 points from 0388.HK.csv
High Res timer: True, freq: 1000000000
Got 3 formulas
Read time series with 247 points
Parsed 3 formulas in 3.552155 ms
Evaluated formulas in 4.36352 ms
root@3d3db6bd3491:/App# 
root@3d3db6bd3491:/App# ls -lart
total 452
-rwxrw-r-- 1 root root 192000 Mar 11 22:26 Antlr4.Runtime.Standard.dll
-rw-rw-r-- 1 root root  18326 Jun  8 16:42 test_input.csv
-rw-rw-r-- 1 root root    124 Jun  8 16:42 test_alpha.txt
-rw-rw-r-- 1 root root  18325 Jun  8 16:42 0388.HK.csv
-rw-rw-r-- 1 root root  20280 Jun  8 16:46 Alpha.pdb
-rw-rw-r-- 1 root root  31232 Jun  8 16:46 Alpha.dll
-rw-rw-r-- 1 root root   1024 Jun  8 16:46 Alpha.deps.json
-rwxr-xr-x 1 root root 138528 Jun  8 16:46 Alpha
-rw-rw-r-- 1 root root    139 Jun  8 16:46 Alpha.runtimeconfig.json
drwxr-xr-x 1 root root   4096 Jun  8 16:53 ..
drwxr-xr-x 1 root root   4096 Jun  8 16:53 .
-rw-r--r-- 1 root root  14263 Jun  8 16:53 test_output.txt
```

#### Full build

Build image:
```
gui@monolith:~/devel/CLEAN/nom01$ docker build -t alphafb -f Dockerfile_fullbuild . 
```

Run from full build:
```
gui@monolith:~/devel/CLEAN/nom01$ docker run -it --rm --entrypoint "bash" alphafb
root@513a2f7d5ce6:/app# ./Alpha.CmdLine --benchmark=true 
Starting, alpha input: test_alpha.txt, ts input: test_input.csv, output: test_output.csv
Number of formulas: 3
Read time series data, num points: 247
Parsing 3 formulas [0]: 83.883 ms
Parsing 3 formulas [1]: 0.188 ms
Parsing 3 formulas [2]: 0.132 ms
Parsing 3 formulas [3]: 0.110 ms
Parsing 3 formulas [4]: 0.109 ms
Formula for ALPHA1 => CLOSE - DELAY(CLOSE, 5)
  Parsed as: [BinaryOp: SUB([PriceField: CLOSE], [TsFunction: DELAY(CLOSE, 5)])]

Formula for ALPHA2 => OPEN - DELAY(CLOSE, 5)
  Parsed as: [BinaryOp: SUB([PriceField: OPEN], [TsFunction: DELAY(CLOSE, 5)])]

Formula for ALPHA3 => (HIGH * LOW)^0.5 - SMA(CLOSE, 5)
  Parsed as: [BinaryOp: SUB([BinaryOp: POWER([BinaryOp: MUL([PriceField: HIGH], [PriceField: LOW])], [Number: 0.5])], [TsFunction: SMA(CLOSE, 5)])]

Evaluating 3 formulas on ts with 247 points [0]: 4.089 ms
Evaluating 3 formulas on ts with 247 points [1]: 0.200 ms
Evaluating 3 formulas on ts with 247 points [2]: 0.156 ms
Evaluating 3 formulas on ts with 247 points [3]: 0.158 ms
Evaluating 3 formulas on ts with 247 points [4]: 0.163 ms
root@513a2f7d5ce6:/app# 
```
