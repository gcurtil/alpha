using System;
using Alpha.Core;

namespace Alpha.Functions
{
    public class SmaTsFunctionCalculator : TsFunctionCalculatorBase, ITsFunctionCalculator
    {
        public SmaTsFunctionCalculator(PriceField field, int intArg)
            : base(field, intArg)
        {
            if (intArg < 1)
            {
                throw new ArgumentException($"Invalid window length value, length={intArg}");
            }

            runningSum = 0.0;
        }

        private double runningSum;

        public int WindowLength 
        { 
            get { return this.IntArg; }
        }

        public int MaxLag()
        {
            return WindowLength - 1;
        }

        public double Eval(PriceTS ts, int index)
        {
            // how far back we have to read values from the time series
            int maxLag = WindowLength - 1;
            checkForValidIndex(index, maxLag);

            double tmpSum = 0.0;
            for(int i=index - maxLag; i <= index; i++)
            {
                var price = ts.Series[i];
                var val = FormulaHelper.ReadField(price, Field);
                tmpSum += val;
            }
            tmpSum = tmpSum / WindowLength;
            return tmpSum;
        }


        public void AddNewPoint(PriceRecord point)
        {
            Buffer.Add(point);
            runningSum += FormulaHelper.ReadField(point, Field);

            while (Buffer.Count > MaxLag() + 1)
            {
                runningSum -= FormulaHelper.ReadField(Buffer[0], Field);
                Buffer.RemoveAt(0);
            }            
        }

        public double CurrentValue()
        {
            // We can recompute the whole sum from Buffer
            // every time if we don't keep any other state than the buffer
            double tmpSum = 0.0;
            // foreach(var price in Buffer)
            // {
            //     var val = FormulaHelper.ReadField(price, Field);
            //     tmpSum += val;
            // }

            // Or we  can use the runningSum variable which is maintained in AddNewPoint().            
            // Note: we could also reset runningSum periodically by recomputing the sum 
            // of the elements in Buffer to avoid accumulating too much rounding error.
            tmpSum = runningSum;

            return tmpSum / WindowLength;
            
        }

    }
        
}