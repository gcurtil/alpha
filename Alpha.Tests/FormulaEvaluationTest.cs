using System;
using System.Collections.Generic;

using NUnit.Framework;
using Alpha.FormulaTree;
using Alpha.Core;

namespace Alpha.Tests
{

    [TestFixture]
    public class FormulaEvaluationTest : FormulaTestsBase
    {

        [Test]
        public void TestFormulaMaxLagValues()
        {            
            var vr = FormulaHelper.ParseFormula("HIGH - LOW");            
            Assert.AreEqual(0, vr.RootNode.MaxLag());

            vr = FormulaHelper.ParseFormula("DELAY(CLOSE, 5)");
            Assert.AreEqual(5, vr.RootNode.MaxLag());

            vr = FormulaHelper.ParseFormula("HIGH - DELAY(CLOSE, 3)");
            Assert.AreEqual(3, vr.RootNode.MaxLag());

            vr = FormulaHelper.ParseFormula("SMA(CLOSE, 5)");
            Assert.AreEqual(4, vr.RootNode.MaxLag());

            vr = FormulaHelper.ParseFormula("DELAY(CLOSE, 5) - SMA(CLOSE, 5)");
            Assert.AreEqual(5, vr.RootNode.MaxLag());
        }        


        [Test]
        public void TestEvalFormulaList()
        {            
            var vr1 = FormulaHelper.ParseFormula("CLOSE - DELAY(CLOSE, 1)");
            var vr2 = FormulaHelper.ParseFormula("HIGH - LOW");            
            var formulaList = new List<IFormulaNode>{ vr1.RootNode, vr2.RootNode };

            var res = FormulaHelper.EvaluateFormulaList(this.ts1, formulaList);
            Assert.AreEqual(2, res.Series.Count);

            // Check the date on the output time series
            Assert.AreEqual(ts1.Series[1].Date, res.Series[0].Date);
            Assert.AreEqual(ts1.Series[2].Date, res.Series[1].Date);

            // Check that we have two alphas for each date
            Assert.AreEqual(2, res.Series[0].Alphas.Count);
            Assert.AreEqual(2, res.Series[1].Alphas.Count);
            
            // Check the alpha values 
            Assert.AreEqual(new double[] { 1.0, 6.0 }, res.Series[0].Alphas);
            Assert.AreEqual(new double[] { 2.0, 7.0 }, res.Series[1].Alphas);
        }                


        [Test]
        public void TestEvalFormulaSimple()
        {            
            var vr = FormulaHelper.ParseFormula("HIGH - LOW");
            var val0 = vr.RootNode.Eval(this.ts1, 0);
            Assert.AreEqual(5.0, val0);
            var val1 = vr.RootNode.Eval(this.ts1, 1);
            Assert.AreEqual(6.0, val1);
            var val2 = vr.RootNode.Eval(this.ts1, 2);
            Assert.AreEqual(7.0, val2);
        }

        [Test]
        public void TestEvalFormulaAlpha1()
        {            
            var vr = FormulaHelper.ParseFormula("CLOSE - DELAY(CLOSE, 1)");
            
            // Check that we have a max lag of 1 as expected, i.e. we need to go 
            // back one point in the past before the current index.
            Assert.AreEqual(1, vr.RootNode.MaxLag());

            // Hence we cannot evaluate the formula on the first point of the 
            // time series, at index == 0
            var ex = Assert.Throws<ArgumentException>(
                () => { vr.RootNode.Eval(this.ts1, 0);  }
            );

            // For index >= 1, we should be able to compute the formula 
            var val1 = vr.RootNode.Eval(this.ts1, 1);
            Assert.AreEqual(1.0, val1); // 102.0 - 101.0
            var val2 = vr.RootNode.Eval(this.ts1, 2);
            Assert.AreEqual(2.0, val2); // 104.0 - 102.0
        }


        public static PriceTS CreateRandomPriceTS(int n, DateTime initialDate, double initialValue)
        {
            var rng = new Random(0);
            var prices = new List<PriceRecord>(n);

            var x = initialValue;
            var openScale = 0.5;
            var closeScale = 1.0;
            var higLowScale = 2.0;
            for(int i=0; i < n; i++)
            {
                x += closeScale * rng.NextDouble();
                var pr = new PriceRecord()
                {
                    Date  = initialDate.AddDays(i),
                    Open  = x + openScale * rng.NextDouble(),
                    Close = x,
                    Low   = x - higLowScale * rng.NextDouble(),
                    High  = x + higLowScale * rng.NextDouble(),
                };
                prices.Add(pr);
            }
            return new PriceTS(prices);
        }

        [Test]
        public void TestAgainstExplicitFormula()
        {
            var vr1 = FormulaHelper.ParseFormula("CLOSE - DELAY(CLOSE, 1)");
            var vr2 = FormulaHelper.ParseFormula("HIGH - LOW");            
            var formulaList = new List<IFormulaNode>{ vr1.RootNode, vr2.RootNode };

            var n = 10;
            var ts = CreateRandomPriceTS(n, new DateTime(2021, 1, 1), 100.0);
            var res = FormulaHelper.EvaluateFormulaList(ts, formulaList);
            Assert.AreEqual(n-1, res.Series.Count);

            // Explicit Formula implementations for the two formulas above
            Func<PriceTS, int, double> af1 = (ts, index) => {
                return ts.Series[index].Close - ts.Series[index-1].Close; 
            };
            Func<PriceTS, int, double> af2 = (ts, index) => {
                return ts.Series[index].High - ts.Series[index].Low; 
            };

            // Now populate the AlphaTS using the explict formulas above
            var arList = new List<AlphaRecord>();
            for(int i = 1; i < n; i++)
            {
                var alphas = new List<double>{ af1(ts, i), af2(ts, i) };
                var ar = new AlphaRecord()
                {
                    Date = ts.Series[i].Date, 
                    Alphas = alphas,
                };
                arList.Add(ar);
            }
            var res2 = new AlphaTS(arList);

            // Check that the resulting AlphaTS res2 matches the one computed
            // using the parsed formulas.
            Assert.AreEqual(res.Series.Count, res2.Series.Count);
            for(int i = 0; i < res.Series.Count; i++)
            {
                Assert.AreEqual(res.Series[i].Date, res2.Series[i].Date);
                Assert.AreEqual(res.Series[i].Alphas, res2.Series[i].Alphas);
            }
        }

        [Test]
        [DefaultFloatingPointTolerance(1e-12)]
        public void TestEvalStatefulVsStateless()
        {
            var vr1 = FormulaHelper.ParseFormula("CLOSE - DELAY(CLOSE, 1)");
            var vr2 = FormulaHelper.ParseFormula("CLOSE - SMA(CLOSE, 3)");
            var formulaList = new List<IFormulaNode> { vr1.RootNode, vr2.RootNode };

            var n = 10;
            var ts = CreateRandomPriceTS(n, new DateTime(2021, 1, 1), 100.0);

            Assert.AreEqual(2, vr2.RootNode.MaxLag());
            var nOut = n - 2; // expected size of output TS, since Max(MaxLag) == 2

            // use Stateless evaluation model            
            var res1 = FormulaHelper.EvaluateFormulaList(ts, formulaList);
            Assert.AreEqual(nOut, res1.Series.Count);

            // use Stateful evaluation model
            var res2 = FormulaHelper.EvaluateFormulaListStateful(ts, formulaList);
            Assert.AreEqual(nOut, res2.Series.Count);

            // Compare the output series point by point.
            // Note that the alphas will have a small difference due to 
            // floating point rounding errors due to the different ways to compute 
            // the SMA() function, hence the need for the DefaultFloatingPointTolerance
            // attribute on the Test method.
            for (int i = 0; i < nOut; i++)
            {                
                var r1 = res1.Series[i];
                var r2 = res2.Series[i];
                Assert.AreEqual(r1.Date, r2.Date);
                Assert.AreEqual(r1.Alphas, r2.Alphas);
            }            
        }
    }
}