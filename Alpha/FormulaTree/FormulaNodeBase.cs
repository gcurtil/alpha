using System;
using System.Collections.Generic;
using Alpha.Core;

namespace Alpha.FormulaTree
{
    public abstract class FormulaNodeBase
    {        
        public FormulaNodeBase()
        {
            this.Children = new List<IFormulaNode>();
        }

        public List<IFormulaNode> Children { get; private set; }
    }

}