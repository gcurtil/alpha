using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Antlr4.Runtime;

using Alpha.Core;
using Alpha.FormulaTree;
using Alpha.Functions;
using Alpha.Generated;


namespace Alpha
{
    public class FormulaHelper
    {
        public static double ReadField(PriceRecord price, PriceField field)
        {
            switch(field)
            {
                case PriceField.OPEN:
                    return price.Open;
                case PriceField.CLOSE:
                    return price.Close;
                case PriceField.LOW:
                    return price.Low;
                case PriceField.HIGH:
                    return price.High;
                default:
                    return 0.0; // Not reached
            }
        }

        
        
        public static VisitResult ParseFormula(string expression)
        {
            using (var stringReader = new StringReader(expression))
            {
                var inputStream = new AntlrInputStream(stringReader);
                var lexer = new AlphaFormulaLexer(inputStream);
                var tokens = new CommonTokenStream(lexer);
                var parser = new AlphaFormulaParser(tokens);
                var expressionTree = parser.expr();// lets call the parser on the whole thing

                var visitor = new MyAlphaFormulaVisitor();
                var visitResult = visitor.Visit(expressionTree);
                
                return visitResult;
            }
        }

        public static AlphaTS EvaluateFormulaList(PriceTS ts, List<IFormulaNode> formulaList)
        {
            var res = new List<AlphaRecord>();
            int p = formulaList.Count;
            if (p == 0)
            {
                return new AlphaTS(res);
            }

            int maxLag = formulaList.Select(x => x.MaxLag()).Max();
            int n = ts.Series.Count;
            res.Capacity = n - maxLag;

            for (int i=maxLag; i < n; i++)
            {
                var alphas = formulaList.Select(x => x.Eval(ts, i)).ToList();
                var ar = new AlphaRecord(){ Date = ts.Series[i].Date, Alphas = alphas };
                res.Add(ar);
            }

            return new AlphaTS(res);
        }

        public static AlphaTS EvaluateFormulaListStateful(
            PriceTS ts, List<IFormulaNode> formulaList)
        {
            var res = new List<AlphaRecord>();
            int p = formulaList.Count;
            if (p == 0)
            {
                return new AlphaTS(res);
            }

            int maxLag = formulaList.Select(x => x.MaxLag()).Max();
            int n = ts.Series.Count;
            res.Capacity = n - maxLag;

            for (int i=0; i < n; i++)
            {
                var point = ts.Series[i];
                foreach (var formula in formulaList)
                {
                    formula.AddNewPoint(point);
                }

                if (i >= maxLag)
                {
                    var alphas = formulaList.Select(x => x.CurrentValue()).ToList();
                    var ar = new AlphaRecord(){ Date = ts.Series[i].Date, Alphas = alphas };
                    res.Add(ar);
                }
            }

            return new AlphaTS(res);
        }
    }

}