using System;
using System.Collections.Generic;

namespace Alpha.Core
{
    public class AlphaRecord : IEquatable<AlphaRecord>
    {
        public DateTime Date { get; set; }

        public List<double> Alphas { get; set; }

        public override bool Equals(object obj)
        {
            return obj is AlphaRecord record &&
                   Date == record.Date &&
                   EqualityComparer<List<double>>.Default.Equals(Alphas, record.Alphas);
        }

        public bool Equals(AlphaRecord other)
        {
            return Equals((object) other);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Date, Alphas);
        }
    }

}