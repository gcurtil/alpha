using System;
using Alpha.Core;

namespace Alpha.Functions
{
    public class TsFunctionCalculatorFactory
    {
        public static ITsFunctionCalculator CreateTsFuncCalculator(TsFunction function, PriceField field, int intArg)
        {
            switch(function)
            {
                case TsFunction.DELAY:
                    return new DelayTsFunctionCalculator(field, intArg);
                case TsFunction.SMA:
                    return new SmaTsFunctionCalculator(field, intArg);
                default:
                    throw new NotImplementedException(""); // not reached
            }
        }
    }
}