using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using Alpha.Core;

namespace Alpha.Util
{
    public class IoHelper
    {
        public static PriceTS ReadPriceHistoryFromCsv(TextReader reader)
        {
            // Headers are:
            // Date,Open,High,Low,Close,Adj Close,Volume
            var records = new List<PriceRecord>();
            var line = "";
            var lineNum = 0;
            while ((line = reader.ReadLine()) != null)
            {
                lineNum++;
                if (lineNum == 1)
                {
                    continue; // skip the header row
                }

                var tokens = line.Split(",");
                DateTime date = DateTime.ParseExact(tokens[0], "yyyy-MM-dd", CultureInfo.InvariantCulture);
                var open = double.Parse(tokens[1]);
                var high = double.Parse(tokens[2]);
                var low = double.Parse(tokens[3]);
                var close = double.Parse(tokens[4]);
                var pr = new PriceRecord()
                {
                    Date = date,
                    Open = open,
                    Close = close,
                    Low = low,
                    High = high
                };
                records.Add(pr);
            }
            return new PriceTS(records);
        }


        public static void WriteAlphaTsToCsv(AlphaTS ts, TextWriter writer)
        {

            var n = ts.Series.Count;
            if (n == 0)
            {
                // nothing to do
                return;
            }

            // Series is not empty, read the first element to see 
            // how many columns we have.
            int p = ts.Series[0].Alphas.Count;

            // Headers are:
            // Date, Alpha1, Alpha2, ...
            var headers = new List<string>();
            headers.Add("Date");
            for (int j = 0; j < p; j++)
            {
                headers.Add($"Alpha{j + 1}");
            }

            // Write header row
            writer.WriteLine(string.Join(",", headers));

            // Write one row for each record
            foreach (var r in ts.Series)
            {
                var vals = new List<string>(p + 1);
                var dateStr = r.Date.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture);
                vals.Add(dateStr);
                vals.AddRange(r.Alphas.Select(x => x.ToString(CultureInfo.InvariantCulture)));
                writer.WriteLine(string.Join(",", vals));
            }
        }

        public static List<Tuple<string, string>> ReadFormulaDefinitions(TextReader reader)
        {
            var res = new List<Tuple<string, string>>();
            var line = "";
            while ((line = reader.ReadLine()) != null)
            {
                if (string.IsNullOrWhiteSpace(line) ||
                    Regex.Match(line, @"\s*(#|//)", RegexOptions.IgnoreCase).Success)
                {
                    // skip empty lines and comment 
                    continue;
                }
                Match m = Regex.Match(line, @"\s*(ALPHA\d+)\s*=\s*(\S.*)\s*$");
                if (m.Success)
                {
                    var name = m.Groups[1].Value;
                    var expression = m.Groups[2].Value;
                    res.Add(Tuple.Create(name, expression));
                }
            }
            return res;
        }
    }

}