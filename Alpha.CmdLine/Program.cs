﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

using System.CommandLine;
using System.CommandLine.Invocation;

using Alpha;
using Alpha.Core;
using Alpha.FormulaTree;
using Alpha.Util;

namespace Alpha.CmdLine
{
    class Program
    {
        static void RunAlpha(string alphaInputPath, string tsInputPath, 
                            string outputPath, bool benchmark)
        {
            //
            Console.WriteLine($"Starting, alpha input: {alphaInputPath}, ts input: {tsInputPath}, output: {outputPath}");
            using (var reader1 = new StreamReader(alphaInputPath))
            using (var reader2 = new StreamReader(tsInputPath))
            { 
                // Read Formula definitions and price time series data from input files
                var defs = IoHelper.ReadFormulaDefinitions(reader1);
                var ts = IoHelper.ReadPriceHistoryFromCsv(reader2);
                var numFormulas = defs.Count;
                var numPoints = ts.Series.Count;
                var numIterations = benchmark ? 5 : 1;
                Console.WriteLine($"Number of formulas: {numFormulas}");
                Console.WriteLine($"Read time series data, num points: {numPoints}");

                // Parse the textual formulas and dump parsed results to console
                List<IFormulaNode> formulaList = null;
                for(int i=0; i < numIterations; i++)
                {
                    using(new LogTimer($"Parsing {numFormulas} formulas [{i}]"))
                    {
                        formulaList = defs.Select(t => FormulaHelper.ParseFormula(t.Item2).RootNode).ToList();
                    }
                }
                for(int i=0; i < numFormulas; i++)
                {                                        
                    Console.WriteLine("Formula for {0} => {1}", defs[i].Item1, defs[i].Item2);
                    Console.WriteLine("  Parsed as: {0}", formulaList[i]);
                    Console.WriteLine("");
                }

                // Now evaluate the parsed formulas on time series input to create AlphaTs output
                AlphaTS outputTs = null;
                for(int i=0; i < numIterations; i++)
                {
                    using(new LogTimer($"Evaluating {numFormulas} formulas on ts with {numPoints} points [{i}]"))
                    {
                        outputTs = FormulaHelper.EvaluateFormulaList(ts, formulaList);
                    }
                }

                // Save output to CSV file
                using (var writer = new StreamWriter(outputPath))
                {
                    IoHelper.WriteAlphaTsToCsv(outputTs, writer);
                }
            }
        }

        static int Main(string[] args)
        {
            // setup command line options 
            var rootCommand = new RootCommand
            {            
                new Option(
                    "--alpha",
                    getDefaultValue: () => "test_alpha.txt",
                    description: "The path of the file for the definition of the alpha formulas"
                ),
                new Option(
                    "--input",
                    getDefaultValue: () => "test_input.csv",
                    description: "The path of the CSV file for price time series input"
                ),
                new Option(
                    "--output",
                    getDefaultValue: () => "test_output.csv",
                    description: "The path of the CSV file for output"
                ),
                new Option<bool>(
                    "--benchmark",
                    getDefaultValue: () => false,
                    description: "whether to repeat the operations a few times for benchmarking"
                )
            };
            rootCommand.Description = "A command-line runner for Alpha";

            rootCommand.Handler = CommandHandler.Create<string, string, string, bool>(
                (alpha, input, output, benchmark) =>
            {
                // invoke our main function                
                RunAlpha(alpha, input, output, benchmark);
            });

            return rootCommand.Invoke(args);
        }
    }
}
