
using NUnit.Framework;
using Alpha.FormulaTree;
using Alpha.Functions;
using Alpha.Core;

namespace Alpha.Tests
{
    [TestFixture]
    public class FormulaParsingTest : FormulaTestsBase
    {
        [Test]
        public void TestParseBinaryFormula()
        {
            var expression = "HIGH - LOW";
            var vr = FormulaHelper.ParseFormula(expression);
            var rootNode = vr.RootNode;
            Assert.IsInstanceOf<BinaryOpFormulaNode>(rootNode);

            // check root node
            BinaryOpFormulaNode n1 = rootNode as BinaryOpFormulaNode;
            Assert.IsNotNull(n1);

            // check left child
            Assert.IsInstanceOf<PriceFieldFormulaNode>(n1.LeftOperand);
            PriceFieldFormulaNode nLeft = n1.LeftOperand as PriceFieldFormulaNode;
            Assert.AreEqual(PriceField.HIGH, nLeft.Field);

            // check right child
            Assert.IsInstanceOf<PriceFieldFormulaNode>(n1.RightOperand);
            PriceFieldFormulaNode nRight = n1.RightOperand as PriceFieldFormulaNode;
            Assert.AreEqual(PriceField.LOW, nRight.Field);
        }

        [Test]
        public void TestParseFormulaWithMultiply()
        {
            var expression = "0.5 * (HIGH + LOW)";
            var rootNode = FormulaHelper.ParseFormula(expression).RootNode;            
            BinaryOpFormulaNode n1 = rootNode as BinaryOpFormulaNode;
            Assert.IsNotNull(n1);
            Assert.AreEqual(BinaryOperation.MUL, n1.Operation);

            Assert.IsInstanceOf<NumberFormulaNode>(n1.LeftOperand);
            NumberFormulaNode nLeft = n1.LeftOperand as NumberFormulaNode;
            Assert.AreEqual(0.5, nLeft.NumValue);

            Assert.IsInstanceOf<BinaryOpFormulaNode>(n1.RightOperand);
            BinaryOpFormulaNode nRight = n1.RightOperand as BinaryOpFormulaNode;
            Assert.AreEqual(BinaryOperation.ADD, nRight.Operation);
        }

        [Test]
        public void TestParseFormulaWithPower()
        {
            var expression = "(HIGH - LOW)^2.0";
            var rootNode = FormulaHelper.ParseFormula(expression).RootNode;            
            BinaryOpFormulaNode n1 = rootNode as BinaryOpFormulaNode;
            Assert.IsNotNull(n1);
            Assert.AreEqual(BinaryOperation.POWER, n1.Operation);

            Assert.IsInstanceOf<BinaryOpFormulaNode>(n1.LeftOperand);
            BinaryOpFormulaNode nLeft = n1.LeftOperand as BinaryOpFormulaNode;
            Assert.AreEqual(BinaryOperation.SUB, nLeft.Operation);

            Assert.IsInstanceOf<NumberFormulaNode>(n1.RightOperand);
            NumberFormulaNode nRight = n1.RightOperand as NumberFormulaNode;
            Assert.AreEqual(2.0, nRight.NumValue);
        }

        [Test]
        public void TestCreateFormulaWithTsFuncDelay()
        {            
            var vr = FormulaHelper.ParseFormula("DELAY(CLOSE, 5)");

            Assert.IsInstanceOf<TsFunctionFormulaNode>(vr.RootNode);            
            TsFunctionFormulaNode n1 = vr.RootNode as TsFunctionFormulaNode;
            Assert.IsNotNull(n1);

            Assert.AreEqual(TsFunction.DELAY, n1.Function);
            Assert.AreEqual(PriceField.CLOSE, n1.Field);
            Assert.AreEqual(5, n1.IntArg);
        }

        [Test]
        public void TestCreateFormulaWithTsFuncSma()
        {            
            var vr = FormulaHelper.ParseFormula("SMA(OPEN, 3)");

            Assert.IsInstanceOf<TsFunctionFormulaNode>(vr.RootNode);            
            TsFunctionFormulaNode n1 = vr.RootNode as TsFunctionFormulaNode;
            Assert.IsNotNull(n1);

            Assert.AreEqual(TsFunction.SMA, n1.Function);
            Assert.AreEqual(PriceField.OPEN, n1.Field);
            Assert.AreEqual(3, n1.IntArg);
        }
    }
}