using System;
using System.Diagnostics;

namespace Alpha.Util
{
    public class LogTimer : IDisposable
    {
        private string fmt;
        private object[] args;
        private Stopwatch sw;

        public LogTimer(string fmt, params object[] args)
        {
            this.fmt = fmt;
            this.args = args;
            this.sw = Stopwatch.StartNew();
        }
        
        public void Dispose()
        {
            this.sw.Stop();

            // base message from the fmt string and arguments passed in
            var baseMsg = string.Format(fmt, this.args);
            // elapsed time in ms
            var dt = 1000.0 * sw.ElapsedTicks / Stopwatch.Frequency;
            Console.WriteLine("{0}: {1:F3} ms", baseMsg, dt);
        }
    }
}