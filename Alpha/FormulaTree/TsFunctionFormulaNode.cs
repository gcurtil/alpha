using System;
using Alpha.Core;
using Alpha.Functions;

namespace Alpha.FormulaTree
{
    public class TsFunctionFormulaNode : FormulaNodeBase, IFormulaNode
    {
        public TsFunctionFormulaNode(TsFunction function, PriceField field, int intArg)
        {
            Function = function;
            Field = field;
            IntArg = intArg;

            _calculator = TsFunctionCalculatorFactory.CreateTsFuncCalculator(function, field, intArg);
        }

        public TsFunction Function { get; }
        public PriceField Field { get; }
        public int IntArg { get; }

        private ITsFunctionCalculator _calculator;

        
        public virtual int MaxLag()
        {
            return _calculator.MaxLag();
        }

        public virtual double Eval(PriceTS ts, int index)
        {
            return _calculator.Eval(ts, index);
        }

        public virtual void AddNewPoint(PriceRecord point)
        {            
            _calculator.AddNewPoint(point);
        }

        public virtual double CurrentValue()
        {
            return _calculator.CurrentValue();
        }

        public override string ToString()
        {
            return string.Format("[TsFunction: {0}({1}, {2})]",
                                 Function, Field, IntArg);
        }
    }

}