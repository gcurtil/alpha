using System.Collections.Generic;
using Alpha.Core;

namespace Alpha.FormulaTree
{
    /// <summary>
    /// Interface for the nodes in the Tree representing a Formula.
    /// </summary>
    public interface IFormulaNode
    {
        /// <summary>
        /// How far back in the time series do we need to look.
        /// </summary>
        /// <returns>The offset from current index in the time series of the earliest point needed.</returns>
        int MaxLag();

        /// <summary>
        /// List of child nodes in the tree.
        /// </summary>
        List<IFormulaNode> Children { get; }

        /// Stateless model for evaluation.
        /// This corresponds to an "on-demand" model, where the full time-series data 
        /// is provided to the node to compute each point, hence nodes do not keep t
        /// keep any internal state.
        
        /// <summary>
        /// Evaluate the node using full input time series and current index (node is (can be) stateless).
        /// </summary>
        /// <param name="ts">the input time series</param>
        /// <param name="index">the current index we want to compute</param>
        /// <returns>value of the node at the specified index</returns>
        double Eval(PriceTS ts, int index);


        // Stateful model for evaluation.
        // This corresponds to a "streaming" model where values are feed incrementally 
        // and nodes can maintain a state as new points are added. 

        void AddNewPoint(PriceRecord point);
        double CurrentValue();
    }

}