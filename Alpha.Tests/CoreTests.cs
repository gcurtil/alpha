using System;
using System.Collections.Generic;

using NUnit.Framework;
using Alpha.Core;

namespace Alpha.Tests
{
    [TestFixture]
    public class CoreTests : FormulaTestsBase
    {
        
        [Test]
        public void TestPriceTS()
        {
            var p1 = new PriceRecord
            {
                Date = new DateTime(2021, 1, 1),
                Open = 100.0, Close = 101.0, Low = 97.0, High = 102.0
            };
            var p2 = new PriceRecord
            {
                Date = new DateTime(2021, 1, 2),
                Open = 101.0, Close = 102.0, Low = 99.0, High = 105.0
            };

            Assert.AreEqual(100.0, p1.Open);
            Assert.AreEqual(101.0, p1.Close);
            Assert.AreEqual(97.0, p1.Low);
            Assert.AreEqual(102.0, p1.High);

            var prices = new List<PriceRecord>(){p1, p2};
            var ts = new PriceTS(prices);

            Assert.AreEqual(2, ts.Series.Count);
            Assert.AreSame(p1, ts.Series[0]);
            Assert.AreSame(p2, ts.Series[1]);
        }
    }
}