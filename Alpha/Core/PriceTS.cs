using System.Collections.Generic;

namespace Alpha.Core
{
    public class PriceTS 
    {
        public PriceTS(IList<PriceRecord> prices)
        {
            this.Series = new List<PriceRecord>(prices);
        }

        public List<PriceRecord> Series { get; }
    }

}