namespace Alpha.Core
{
    public enum PriceField
    {
        OPEN,
        CLOSE,
        HIGH,
        LOW
    }

}