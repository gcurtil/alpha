namespace Alpha.Core
{
    public enum BinaryOperation
    {
        ADD,
        SUB,
        MUL,
        DIV,
        POWER,
    }

}