using System.Collections.Generic;

namespace Alpha.Core
{
    public class AlphaTS
    {
        public AlphaTS(IList<AlphaRecord> alphas)
        {
            Series = new List<AlphaRecord>(alphas);
        }

        public List<AlphaRecord> Series { get; }
    }

}