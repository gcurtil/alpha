using System;
using System.Collections.Generic;

using NUnit.Framework;
using Alpha.Core;

namespace Alpha.Tests
{
    [TestFixture]
    public class FormulaTestsBase
    {
        [SetUp]
        public void Setup()
        {
            var p1 = new PriceRecord
            {
                Date = new DateTime(2021, 1, 1), Open = 100.0, Close = 101.0, Low = 97.0, High = 102.0
            };
            var p2 = new PriceRecord
            {
                Date = new DateTime(2021, 1, 2), Open = 101.0, Close = 102.0, Low = 99.0, High = 105.0
            };
            var p3 = new PriceRecord
            {
                Date = new DateTime(2021, 1, 3), Open = 100.0, Close = 104.0, Low = 98.0, High = 105.0
            };

            var prices = new List<PriceRecord>(){p1, p2, p3};
            this.ts1 = new PriceTS(prices);
        }

        protected PriceTS ts1;
    }
}