FROM mcr.microsoft.com/dotnet/aspnet:5.0

COPY Alpha.CmdLine/bin/release/net5.0/publish/ App/
COPY Alpha.CmdLine/test_alpha.txt Alpha.CmdLine/test_input.csv App/

WORKDIR /App
ENTRYPOINT ["dotnet", "Alpha.CmdLine.dll"]

ENV DOTNET_EnableDiagnostics=0
