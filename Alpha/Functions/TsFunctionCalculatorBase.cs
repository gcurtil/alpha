using System;
using System.Collections.Generic;
using Alpha.Core;

namespace Alpha.Functions
{
    public class TsFunctionCalculatorBase
    {
        public TsFunctionCalculatorBase(PriceField field, int intArg)
        {
            this.Field = field;
            this.IntArg = intArg;
            this.Buffer = new List<PriceRecord>(intArg);
        }

        public PriceField Field { get; }
        public int IntArg { get; }
        public List<PriceRecord> Buffer { get;  }

        public void checkForValidIndex(int index, int maxLag)
        {
            var effectiveIndex = index - maxLag;
            if (effectiveIndex < 0) 
            {
                throw new ArgumentException($"Invalid index requested, index={index}, maxLag={maxLag}");
            }   
        }
    }
        
}