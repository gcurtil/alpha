using Alpha.Core;

namespace Alpha.Functions
{
    public interface ITsFunctionCalculator
    {
        int MaxLag();

        double Eval(PriceTS ts, int index);
        
        void AddNewPoint(PriceRecord point);
        double CurrentValue();
    }
        
}