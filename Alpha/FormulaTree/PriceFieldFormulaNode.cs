using System;
using Alpha.Core;

namespace Alpha.FormulaTree
{
    public class PriceFieldFormulaNode : FormulaNodeBase, IFormulaNode
    {

        public PriceField Field { get; set; }
        private PriceRecord currentPoint;

        public virtual int MaxLag()
        {
            return 0;
        }

        public virtual double Eval(PriceTS ts, int index)
        {
            var price = ts.Series[index];
            return FormulaHelper.ReadField(price, Field);
        }

        public virtual void AddNewPoint(PriceRecord point)
        {
            this.currentPoint = point;
        }

        public virtual double CurrentValue()
        {
            return FormulaHelper.ReadField(this.currentPoint, Field);
        }

        public override string ToString()
        {
            return string.Format("[PriceField: {0}]", Field);
        }
    }

}